import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao Mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Super SS",
    },
    {
      id: 2,
      title: "Ir ao SUPERmercado",
      date: "2024-02-27",
      time: "11:00",
      address: "Super SG",
    },
    {
      id: 3,
      title: "Ir na Balada",
      date: "2024-02-27",
      time: "12:00",
      address: "Clean Clean",
    },
  ];
const taskPress=(task)=>{
navigation.navigate('DetalhesDasTarefas',{task});
}
  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.idtoString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
